run: ## run project
	go run cmd/main.go
	
folder: ## create folder in project
	@mkdir -p cmd
	@mkdir -p config
	@mkdir -p db
	@mkdir -p docs
	@mkdir -p genproto
	@mkdir -p internal
	@mkdir -p internal/adapter
	@mkdir -p internal/api
	@mkdir -p internal/common
	@mkdir -p internal/dto
	@mkdir -p internal/helper
	@mkdir -p internal/registry
	@mkdir -p internal/repository
	@mkdir -p internal/usecase
	@mkdir -p internal/utils
	@mkdir -p proto
	@mkdir -p sql

mod: ## install go library
	@go mod tidy 
	@go mod download 
	@go mod vendor
dep: ## download dependency important
	export GO111MODULE=on 
	go get google.golang.org/protobuf/cmd/protoc-gen-go \
        google.golang.org/grpc/cmd/protoc-gen-go-grpc \
		github.com/golang/protobuf/protoc-gen-go \
		github.com/grpc-ecosystem/grpc-gateway \
		google.golang.org/grpc \
		github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
		github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway \
		github.com/mwitkow/go-proto-validators/protoc-gen-govalidators

generate: ## Generate proto
	@third_party/protoc/bin/protoc \
		-I proto/ \
		-I $(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis/ \
		-I $(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/ \
		-I $(GOPATH)/src/github.com/gogo/protobuf/ \
		-I $(GOPATH)/src/github.com/mwitkow/go-proto-validators/ \
		--gogo_out=plugins=grpc:genproto \
		--govalidators_out=gogoimport=true:genproto \
		--grpc-gateway_out=genproto \
		--swagger_out=docs \
		proto/*.proto


generate-v2: ## Generate proto
	@third_party/protoc/bin/protoc \
		-I proto/ \
		-I $(GOPATH)/src/github.com/googleapis \
		-I $(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/ \
		-I $(GOPATH)/src/google.golang.org/protobuf \
		-I $(GOPATH)/src/github.com/mwitkow/go-proto-validators/ \
		--go_out=pb \
		--go_opt paths=source_relative \
		--go-grpc_out=pb \
		--go-grpc_opt paths=source_relative \
		--go-grpc_opt require_unimplemented_servers=false \
		--govalidators_out=gogoimport=true:pb \
		--grpc-gateway_out=pb \
		--grpc-gateway_opt paths=source_relative \
		--openapiv2_out ./docs \
    	--openapiv2_opt logtostderr=true \
		proto/*.proto


build:
	@statik -f -src=docs -dest=cmd/server && cd cmd/server && go build -mod=mod -o shopbackclient .

lint: ## Run linter
	golangci-lint run ./...


docker-build:
	sudo docker build -t appbe .

set-external:
	export GOINSECURE="gitlab.com/phucducktpm/*"

# *** container it me ***
docker-run-container-app-back-office:
	docker container run -d -p 10003:8080 -h 0.0.0.0 -v local-cim:/var/local-cim all-cim
# *** jenkin ***
docker-jenkin-img-container:
	docker run --name jenkins-docker --detach --publish 2376:2376 jenkins/jenkins:latest
	# or
	docker container run -d -p 2376:8080 -v jenkin_local:var/jenkins_home --name jenkins-local images_name
docker-create-volumn-jenkins:
	docker volume create jenkin_local
docker-run-jenkins: # run image jenkin -> container
	docker container run -d -p 2376:8080 -v jenkin_local:/var/jenkins_home --name jenkins-local jenkins/jenkins:latest
docker-create-pass-jenkins: # docker container exec  5b550329ef43 sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"
	docker container exec  [CONTAINER ID or NAME]   sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"

# *** mysql ***