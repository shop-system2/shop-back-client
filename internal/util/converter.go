package util

import "strconv"

func ConvertStringToBool(boolStr string) bool {
	if boolVal, err := strconv.ParseBool(boolStr); err == nil {
		return boolVal
	}
	switch boolStr {
	case "Y":
		return true
	case "N":
		return false
	}
	return false
}

func ConvertBoolToString(val bool) string {
	switch val {
	case true:
		return "Y"
	case false:
		return "N"
	}
	return ""
}

func ConvertStringToUint32(val string) uint32 {
	var i uint64
	i, err := strconv.ParseUint(val, 2, 32)
	if err != nil {
		return 0
	}
	u32 := uint32(i)
	return u32
}

func ConvertUint64ToString(u64 int64) string {
	str := strconv.FormatInt(u64, 10)
	return str
}

func ConvertStringToInt64(intStr string) int64 {
	val, err := strconv.ParseInt(intStr, 10, 64)
	if err != nil {
		return 0
	}
	return val
}

func ConvertUint64To32(in uint64) uint32 {
	inStr := strconv.FormatUint(in, 10)
	uin64, err := strconv.ParseUint(inStr, 10, 32)
	if err != nil {
		return 0
	}
	return uint32(uin64)
}
