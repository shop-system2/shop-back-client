package util

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"math/rand"
	"strings"
	"time"
)

var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func NowAsUnixSecondNano() int64 {
	return time.Now().UnixNano()
}

func nowAsUnixSecond() int64 {
	return time.Now().Unix()
}

func getRefreshToken() string {
	var id strings.Builder
	dest, _ := hex.DecodeString(fmt.Sprintf("%d", NowAsUnixSecondNano()))
	encode := base64.StdEncoding.EncodeToString(dest)
	//rand.Seed(nowAsUnixSecondNano())
	id.WriteString(encode)
	id.WriteString(RandString(10))
	return strings.Replace(id.String(), ".", RandString(10), 1)
}

func GetID() string {
	var id strings.Builder
	dest, _ := hex.DecodeString(fmt.Sprintf("%d", NowAsUnixSecondNano()))
	encode := base64.StdEncoding.EncodeToString(dest)
	//rand.Seed(nowAsUnixSecondNano())
	id.WriteString(encode)
	id.WriteString(RandString(5))
	return strings.Replace(id.String(), ".", RandString(5), 1)
}
