package util

import (
	"reflect"
	"shop-back-client/internal/common"
)

func SetErrorToResponse(err error, in interface{}) {
	if err == nil || reflect.TypeOf(in).Kind() != reflect.Ptr {
		return
	}
	reason := common.ParseError(err)
	values := reflect.ValueOf(in)
	vIndirect := reflect.Indirect(values)
	reasonCode := vIndirect.FieldByName("ReasonCode")
	status := vIndirect.FieldByName("StatusCode")
	reasonMessage := vIndirect.FieldByName("ReasonMessage")
	if reasonCode.CanSet() {
		reasonCode.SetString(reason.Code())
	}
	if status.CanSet() {
		status.SetString(common.FAILED)
	}
	if reasonMessage.CanSet() {
		reasonMessage.SetString(reason.Message())
	}
}
