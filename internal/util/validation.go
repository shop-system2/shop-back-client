package util

import (
	"regexp"
)

// ValidateEmail validation email using regex
func ValidateEmail(e string) bool {
	if m, _ := regexp.MatchString("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", e); !m {
		return false
	}
	return true
}
