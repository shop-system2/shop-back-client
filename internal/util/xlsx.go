package util

import (
	"bufio"
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"go-libs/logger"
	"reflect"
	"shop-back-client/internal/common"
	"strconv"
	"strings"
	"time"

	"go.uber.org/zap"

	"github.com/tealeg/xlsx/v3"
)

func Base64FileData(ctx context.Context, file *xlsx.File) (base64Str string, err error) {
	var b bytes.Buffer
	writr := bufio.NewWriterSize(&b, 4096*2)
	logger.GlobaLogger.Info("Starting writing file")
	err = file.Write(writr)
	logger.GlobaLogger.Info("Stopped writing file")
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Write file error: ", zap.Error(err))
		return "", err
	}
	logger.GlobaLogger.Info("Starting encoding file")
	stringEncoded := base64.RawStdEncoding.EncodeToString(b.Bytes())
	writr.Flush()
	logger.GlobaLogger.Info("Stopped encoding file")
	return stringEncoded, nil
}

func WriteDataInFile(ctx context.Context, reportName, domainUser string, header []string,
	cellValues, additionalData interface{}) (file *xlsx.File, err error) {
	file = xlsx.NewFile()
	sh, err := file.AddSheet("Report")
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Addsheet error: ", zap.Error(err))
		return nil, err
	}
	writeTitleInfoToSheet(reportName, domainUser, sh)
	writeValueInfo(additionalData, sh)
	writeHeaderRowToSheet(header, sh)
	writeCellValueToSheet(cellValues, sh)
	return file, nil
}

// ExportToXLSX support for gen xlsx file
func ExportToXLSX(ctx context.Context, reportName, domainUser string, header []string,
	cellValues interface{}) string {
	var err error
	file := xlsx.NewFile()
	sh, err := file.AddSheet("Report")
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Addsheet error: ", zap.Error(err))
		return ""
	}
	writeTitleInfoToSheet(reportName, domainUser, sh)
	writeHeaderRowToSheet(header, sh)
	writeCellValueToSheet(cellValues, sh)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - write data fail: ", zap.Error(err))
		return ""
	}

	var b bytes.Buffer
	writr := bufio.NewWriterSize(&b, 4096*2)
	logger.GlobaLogger.Info("Starting writing file")
	err = file.Write(writr)
	logger.GlobaLogger.Info("Stopped writing file")
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Write file error: ", zap.Error(err))
		return ""
	}
	logger.GlobaLogger.Info("Starting encoding file")
	stringEncoded := base64.RawStdEncoding.EncodeToString(b.Bytes())
	writr.Flush()
	logger.GlobaLogger.Info("Stopped encoding file")
	return stringEncoded
}

func writeTitleInfoToSheet(reportName, domainUser string, sheet *xlsx.Sheet) {
	var cell *xlsx.Cell
	var row *xlsx.Row

	// Add title report name
	titleFont := xlsx.NewFont(16, "Arial")
	titleFont.Bold = true
	titleStyle := xlsx.NewStyle()
	titleStyle.Font = *titleFont
	row = sheet.AddRow()
	row.SetHeight(40)
	cell = row.AddCell()
	cell.SetStyle(titleStyle)
	cell.Value = strings.Replace(reportName, "_", " ", -1)

	// Add infomation
	_ = sheet.AddRow()
	row = sheet.AddRow()
	row.SetHeight(20)
	cell = row.AddCell()
	cell.Value = fmt.Sprintf("Created by: %v", domainUser)
	row = sheet.AddRow()
	row.SetHeight(20)
	cell = row.AddCell()
	cell.Value = fmt.Sprintf("Created Time: %v", time.Now().Format("2006/01/02 15:04:05"))
}

func writeHeaderRowToSheet(columns []string, sheet *xlsx.Sheet) {
	headerFont := xlsx.NewFont(12, "Arial")
	headerFont.Bold = true
	headerStyle := xlsx.NewStyle()
	headerStyle.Font = *headerFont
	var cell *xlsx.Cell
	row := sheet.AddRow()
	row.SetHeight(20)
	for _, col := range columns {
		cell = row.AddCell()
		cell.SetStyle(headerStyle)
		cell.Value = strings.Replace(col, "_", " ", -1)
	}
}

func writeValueToCell(value reflect.Value, cell *xlsx.Cell) {
	if !value.IsValid() {
		return
	}
	switch value.Interface().(type) {
	case string:
		cell.Value = value.String()
	case float64:
		cell.SetFloat(value.Float())
	case int64:
		cell.SetInt64(value.Int())
	default:
		cell.Value = value.String()
	}

}

func writeValueInfo(objInfo interface{}, sheet *xlsx.Sheet) {

	titleFont := xlsx.NewFont(10, "Arial")
	titleFont.Bold = true
	titleStyle := xlsx.NewStyle()
	titleStyle.Font = *titleFont
	object := reflect.ValueOf(objInfo)
	for i := 0; i < object.Len(); i++ {
		row := sheet.AddRow()
		row.SetHeight(20)

		item := object.Index(i)
		switch item.Kind() {
		case reflect.Ptr:
		case reflect.Interface:
			itemsInterface := item.Elem()
			if itemsInterface.Kind() == reflect.Struct {
				var (
					locationX int
					locationY int
					direction string
				)
				for j := 0; j < itemsInterface.NumField(); j++ {

					itemValue := itemsInterface.Field(j)
					name := itemsInterface.Type().Field(j).Name

					if name == "" {
						continue
					}
					if name == common.LocationX {
						locationX = Parsenumber(itemValue)
						continue
					}

					if name == common.LocationY {
						locationY = Parsenumber(itemValue)
						continue
					}

					if name == common.Position {
						continue
					}

					cellKey, _ := sheet.Cell(locationY, locationX)
					newROw, _ := sheet.Row(locationY)
					newROw.AddCell()
					if name == common.Direction {
						direction = itemValue.String()
						continue
					}

					if name == common.Key {
						cellKey.SetStyle(titleStyle)
						writeValueToCell(itemValue, cellKey)
						continue
					}

					if direction == common.PositionHorizontal {
						cellValue, _ := sheet.Cell(locationY, locationX+1)
						writeValueToCell(itemValue, cellValue)
					} else {
						cellValue, _ := sheet.Cell(locationY+1, locationX)
						writeValueToCell(itemValue, cellValue)
					}

				}
			}
		}
	}
}

func Parsenumber(itemValue reflect.Value) int {
	var value int
	switch itemValue.Interface().(type) {
	case string:
		value, _ = strconv.Atoi(itemValue.String())
	case float64:
		value = int(itemValue.Float())
	case int64:
		value = int(itemValue.Int())
	default:
		value, _ = strconv.Atoi(itemValue.String())
	}
	return value
}

func writeCellValueToSheet(cellValues interface{}, sheet *xlsx.Sheet) {
	items := reflect.ValueOf(cellValues)

	if items.Kind() == reflect.Slice {
		for i := 0; i < items.Len(); i++ {
			item := items.Index(i) // Row
			row := sheet.AddRow()
			row.SetHeight(20)
			var cell *xlsx.Cell
			if item.Kind() == reflect.Struct {
				for i := 0; i < item.NumField(); i++ {
					valueField := item.Field(i)
					f := valueField.Interface()
					val := reflect.ValueOf(f)
					cell = row.AddCell() // Cell of row
					writeValueToCell(val, cell)
				}
			}
			if item.Kind() == reflect.Interface {
				itemsInterface := item.Elem()
				if itemsInterface.Kind() == reflect.Slice {
					for j := 0; j < itemsInterface.Len(); j++ {
						cell = row.AddCell() // Cell of row
						itemElem := reflect.Indirect(itemsInterface.Index(j).Elem())
						value := reflect.Indirect(itemElem)
						writeValueToCell(value, cell)
					}
				}
				if itemsInterface.Kind() == reflect.Struct {
					for j := 0; j < itemsInterface.NumField(); j++ {
						cell = row.AddCell() // Cell of row
						itemValue := itemsInterface.Field(j)
						switch itemValue.Interface().(type) {
						case string:
							cell.Value = itemValue.String()
						case float64:
							cell.SetFloat(itemValue.Float())
						case int64:
							cell.SetInt64(itemValue.Int())
						default:
							cell.Value = itemValue.String()
						}
					}
				}
			}
			if item.Kind() == reflect.Ptr {
				itemsInterface := item.Elem()
				if itemsInterface.Kind() == reflect.Slice {
					for j := 0; j < itemsInterface.Len(); j++ {
						cell = row.AddCell() // Cell of row
						itemElem := reflect.Indirect(itemsInterface.Index(j).Elem())
						value := reflect.Indirect(itemElem)
						writeValueToCell(value, cell)
					}
				}
				if itemsInterface.Kind() == reflect.Struct {
					for j := 0; j < itemsInterface.NumField(); j++ {
						cell = row.AddCell() // Cell of row
						itemValue := itemsInterface.Field(j)
						writeValueToCell(itemValue, cell)
					}
				}
			}
		}
	}
}
