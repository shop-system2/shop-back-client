package common

// ReasonCode represents reason code
type ReasonCode string

const (

	// ReasonGeneralError represents reason general error
	ReasonGeneralError ReasonCode = "-1"
	// ReasonDBError represents reason db error
	ReasonDBError ReasonCode = "-2"
	// ReasonCacheError represents reason cache error
	ReasonCacheError ReasonCode = "-3"
	// ReasonExternalAdapterError represents reason adapter error
	ReasonExternalAdapterError ReasonCode = "-4"
	// ReasonNotFound represents reason db not found error
	ReasonNotFound ReasonCode = "-5"
	// ReasonInvalidArgument represents reason invalid argument error
	ReasonInvalidArgument ReasonCode = "-6"
	ReasonPannic          ReasonCode = "-3001"
	// JWT Error - Start from -2001
	ReasonJWTInvalid                             ReasonCode = "-2001"
	ReasonJWTExpired                             ReasonCode = "-2002"
	ReasonNotHasAnyData                          ReasonCode = "-7"
	ReasonNotValidApproveAction                  ReasonCode = "-8"
	ReasonNotFoundMD                             ReasonCode = "-9"
	ReasonDuplicateImportBatch                   ReasonCode = "-10"
	ReasonImportFileFailed                       ReasonCode = "-11"
	ReasonMethodNotSupport                       ReasonCode = "-12"
	ReasonNoRowAffected                          ReasonCode = "-13"
	ReasonReadFileError                          ReasonCode = "-14"
	ReasonFileEmpty                              ReasonCode = "-15"
	ReasonNotMatchDef                            ReasonCode = "-16"
	ReasonNotFoundDataSheet                      ReasonCode = "-17"
	ReasonCMSAccountNotValidForDebitTransfer     ReasonCode = "-18"
	ReasonCMSAccountNotValidForCreditTransfer    ReasonCode = "-19"
	ReasonDuplicateBalanceTransferInday          ReasonCode = "-20"
	ReasonIssuingStatusClosed                    ReasonCode = "-21"
	ReasonCardHolderNameNotMatched               ReasonCode = "-22"
	ReasonExpiryDateNotValid                     ReasonCode = "-23"
	ReasonSetCacheFail                           ReasonCode = "-24"
	ReasonPending                                ReasonCode = "-25"
	ReasonNotFoundInstalmentScheme               ReasonCode = "-26"
	ReasonNotFoundIssuingCardmask                ReasonCode = "-27"
	ReasonNotMatchedTypeWithFee                  ReasonCode = "-28"
	ReasonCreateInstantEntityProductCodeNotFound ReasonCode = "-29"
	ReasonFileIPSNotExists                       ReasonCode = "-30"
	ReasonFailedInitTransaction                  ReasonCode = "-31"
	ReasonProcessFailed                          ReasonCode = "-32"
	ReasonInvalidCardType                        ReasonCode = "-33"
	ReasonNotMatchedCifRegNumber                 ReasonCode = "-34"
	ReasonInvalidReason                          ReasonCode = "-35"
	ReasonInvalidField                           ReasonCode = "-36"
	ReasonRedisNil                               ReasonCode = "-37"
	ReasonProcessErr                             ReasonCode = "-38"
	ReasonStreamFileErr                          ReasonCode = "-39"
	ReasonFileNameIncorrect                      ReasonCode = "-40"
	ReasonRedisPatternIncorrect                  ReasonCode = "-41"
	ReasonConvertTimeFailed                      ReasonCode = "-3000"
	ReasonApprove                                ReasonCode = "1"
	ReasonSuccess                                ReasonCode = "2"
	ReasonAccountRestrictCode                    ReasonCode = "3"
	ReasonLocaleEmptyCode                        ReasonCode = "4"
	ReasonProcessingApprovedData                 ReasonCode = "5"

	// client reason error
	ReasonClientFieldNameNotEmpty ReasonCode = "3100"
	ReasonClientFieldIDInvalid    ReasonCode = "3101"

	// vendor
	ReasonVendorFieldIDInvalid ReasonCode = "3201"
)

var reasonCodeValues = map[string]string{
	"-1":    "General Error",
	"-2":    "DB Error",
	"-3":    "Cache Error",
	"-4":    "Adapter Error",
	"-5":    "Not Found",
	"-6":    "Invalid Argument",
	"-7":    "Not has any data",
	"-8":    "Approve action is not valid",
	"-9":    "Not found user action",
	"-10":   "Duplicate import batch ",
	"-11":   "Import file is failed",
	"-12":   "Method is not supported",
	"-13":   "No Row Affected in DB",
	"-14":   "Reading imported file with error",
	"-15":   "Imported file is empty",
	"-16":   "Model def is not matched",
	"-17":   "Default sheet Data is missing",
	"-18":   "CMS Account is not valid for debit transfer",
	"-19":   "CMS Account is not valid for credit transfer",
	"-20":   "Duplicate balance transfer in day",
	"-21":   "Account Waiting Closed",
	"-22":   "Card holder name is not matched",
	"-23":   "Given expiry date is not valid",
	"-24":   "Failed while set cache",
	"-25":   "Process is being do by someone",
	"-26":   "Invalid Instalment scheme",
	"-27":   "Invalid Issuing number and card mask",
	"-28":   "Not matched type with fee",
	"-29":   "Product code not found",
	"-30":   "File not exists",
	"-31":   "Failed init transaction",
	"-32":   "Processing data fail",
	"-33":   "Invalid card type",
	"-34":   "Not matched cif number and reg number",
	"-35":   "Invalid reason",
	"-36":   "Invalid field",
	"-37":   "Redis err",
	"-38":   "Process err",
	"-39":   "Stream file err",
	"-40":   "File name incorrect",
	"-41":   "Pattern incorrect",
	"-2001": "Invalid JWT",
	"-2002": "JWT is expired",
	"-3000": "Time conversion failed",
	"-3001": "Process is pannic",
	"1":     "This function need wait approve",
	"2":     "Successful Import/ Successful Approve",
	"3":     "Account has been restricted for crediting",
	"4":     "Array locale update empty",
	"5":     "Approval is done, processing data",
	// describe client error
	"3100": "Field name not empty",
	"3101": "Field id invalid",
	"3201": "Field id invalid",
}

// Code represents reason code
func (rc ReasonCode) Code() string {
	return string(rc)
}

// Message represents reason message
func (rc ReasonCode) Message() string {
	if value, ok := reasonCodeValues[rc.Code()]; ok {
		return value
	}
	return string(rc)
}

// ParseError represents parse error
func ParseError(err error) ReasonCode {
	return ReasonCode(err.Error())
}
