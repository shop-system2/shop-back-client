package common

const (
	Key                = "Key"
	Direction          = "Direction"
	LocationY          = "LocationY"
	Position           = "Position"
	PositionHorizontal = "horizontal"
	LocationX          = "LocationX"
)

const (
	// PROCESSING represents processing
	PROCESSING = "PROCESSING"
	// ACCEPT represents accept
	ACCEPT = "ACCEPT"
	// REJECT represents reject
	REJECT = "REJECT"
	// FAILED represents failure
	FAILED = "FAILED"
	// DONE represents done
	DONE = "DONE"
	// DomainMDKey represents domain
	DomainMDKey = "domain"
	// LangVi represents language vn
	LangVi = "vi-VN"
	// LangEn represents language us
	LangEn = "en-US"
	// Report download load file type
	FileExcel = "EXCEL"
	FileCsv   = "CSV"
)
const (
	// GRPCMetadata represent grpc metadata
	GRPCMetadata = "grpc-metadata-%v"
	// CustomHeaderContentType represents grpc metadata
	CustomHeaderContentType = "Custom-Header-Content-Type"
	// CustomHeaderContentDisposition represents grpc metadata
	CustomHeaderContentDisposition = "Custom-Header-Content-Disposition"
	// CustomHeaderFileName represents grpc metadata
	CustomHeaderFileName = "Custom-Header-File-Name"
	// CustomHeaderContentTransferEncoding represents grpc metadata
	CustomHeaderContentTransferEncoding = "Custom-Header-Content-Transfer-Encoding"
	// CustomHeaderAdditionalInfo represents grpc metadata
	CustomHeaderAdditionalInfo = "Custom-Header-Additional-Info"
	// CustomFileType represents grpc metadata
	CustomFileType = "Custom-File-Type"
	// CustomFileAdditionInfo represents grpc metadata
	CustomFileAdditionInfo = "Custom-File-Addition-Info"
	// CustomHeaderFileNameRaw  represents for custom file name raw
	CustomHeaderFileNameRaw = "Custom-Header-File-Name-Raw"
)

// redis key
const (
	KeyCacheGetProductPreferentail = "backclient:product_preferentail"
	KeyCacheGetProductTrending     = "backclient:product_trending"
	KeyCacheGetProductDetail       = "backclient:detail-product:%v"
)
