package usecase

import (
	"context"
	"errors"
	"go-libs/logger"
	"shop-back-client/config"
	"shop-back-client/internal/common"
	"shop-back-client/internal/dto"
	"shop-back-client/internal/helper"
	"shop-back-client/internal/helper/cache"
	"shop-back-client/internal/repository"
	"shop-back-client/internal/repository/model"
)

type (
	// ChatUsecase declare all func in chat usecase
	ChatUsecase interface {
		CreateChatroom(ctx context.Context, req *dto.Chatroom) error
	}
	chatUsecase struct {
		cfg            *config.Config
		chatRepository repository.ChatRepository
		helperRedis    cache.CacheHelper
		modelCopier    helper.ModelCopier
		logger         logger.Logger
	}
)

// NewChatUsecase create instance Comment Usecase
func NewChatUsecase(
	cfg *config.Config,
	chatRepository repository.ChatRepository,
	helperRedis cache.CacheHelper,
	modelCopier helper.ModelCopier,
	logger logger.Logger,
) ChatUsecase {
	return &chatUsecase{
		cfg:            cfg,
		chatRepository: chatRepository,
		helperRedis:    helperRedis,
		modelCopier:    modelCopier,
		logger:         logger,
	}
}

func (u *chatUsecase) CreateChatroom(ctx context.Context, req *dto.Chatroom) error {
	reqModel := &model.Chatroom{}
	u.modelCopier.CopyFromModel(reqModel, req)
	err := u.chatRepository.CreateChatroom(ctx, reqModel)
	if err != nil {
		u.logger.Errorf("%v Error while create chatroom, err: %v", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}
