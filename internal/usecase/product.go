package usecase

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-client/config"
	"shop-back-client/internal/common"
	"shop-back-client/internal/dto"
	"shop-back-client/internal/helper"
	"shop-back-client/internal/helper/cache"
	"shop-back-client/internal/repository"
	"shop-back-client/internal/repository/model"
	"time"
)

type (
	// ProductUsecase declare all func in product usecase
	ProductUsecase interface {
		GetProductPreferentail(ctx context.Context,
			req *dto.GetProductPreferentailRequestDTO) (res []*dto.ProductPreferentailResponseDTO, err error)
		GetProductTrending(ctx context.Context,
			req *dto.GetProductTrendingRequestDTO) (res *dto.ProductTrendingResponseDTO, err error)
		GetProductDetail(ctx context.Context, req *dto.GetProductDetailRequestDTO) (res *dto.GetProductDetailResponseDTO, err error)
	}
	productUsecase struct {
		cfg               *config.Config
		productRepository repository.ProductRepository
		helperRedis       cache.CacheHelper
		modelCopier       helper.ModelCopier
	}
)

// NewProductUsecase create instance Comment Usecase
func NewProductUsecase(
	cfg *config.Config,
	productRepository repository.ProductRepository,
	helperRedis cache.CacheHelper,
	modelCopier helper.ModelCopier,
) ProductUsecase {
	return &productUsecase{
		cfg:               cfg,
		productRepository: productRepository,
		helperRedis:       helperRedis,
		modelCopier:       modelCopier,
	}
}

func (u *productUsecase) GetProductTrending(ctx context.Context,
	req *dto.GetProductTrendingRequestDTO) (resp *dto.ProductTrendingResponseDTO, err error) {
	var (
		modelProduct = []*model.ProductModel{}
	)
	keyCache := fmt.Sprintf(common.KeyCacheGetProductTrending)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelProduct)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v,usecase.ProductUsecase client/GetProductTrending get in Redis", req.TransactionID)
		modelProduct = valueInterface.([]*model.ProductModel)
	} else {
		reqModel := &model.ProductModelReqeust{}
		u.modelCopier.CopyFromModel(reqModel, req)
		modelProduct, err = u.productRepository.GetProductTrending(ctx, reqModel)
		if err != nil {
			logger.GlobaLogger.Errorf("%v,usecase.ProductUsecase client/GetProductTrending: Error, %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		if len(modelProduct) == 0 {
			logger.GlobaLogger.Errorf("%v - Get product trending not found", req.TransactionID)
			return nil, errors.New(common.ReasonNotFound.Code())
		}
		u.helperRedis.SetInterface(ctx, keyCache, modelProduct,
			time.Duration(u.cfg.RedisTimeDuration.CacheTimeProduct)*time.Second)
	}
	resp = &dto.ProductTrendingResponseDTO{}
	resp.ProductTrending = make([]*dto.ProductDTO, len(modelProduct))
	for i, item := range modelProduct {
		tag := dto.ProductTag{
			New: item.Tag,
		}
		amount := dto.ProductAmount{
			Sell:  item.SellPrice,
			Price: item.Price,
		}
		resp.ProductTrending[i] = &dto.ProductDTO{
			ID:      item.ID,
			Content: item.Description,
			Name:    item.Name,
			Tag:     tag,
			ImageID: item.ImageID,
			Amount:  amount,
		}
	}
	return resp, nil
}

func (u *productUsecase) GetProductPreferentail(ctx context.Context,
	req *dto.GetProductPreferentailRequestDTO) (product []*dto.ProductPreferentailResponseDTO, err error) {
	var (
		modelProduct = []*model.ProductPreferentailModel{}
	)
	keyCache := fmt.Sprintf(common.KeyCacheGetProductPreferentail)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelProduct)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v,usecase.ProductUsecase client/GetProductPreferentail get in Redis", req.TransactionID)
		modelProduct = valueInterface.([]*model.ProductPreferentailModel)
	} else {
		modelProduct, err = u.productRepository.GetProductPreferentail(ctx, req)
		if err != nil {
			logger.GlobaLogger.Errorf("%v,usecase.ProductUsecase client/GetProductPreferentail: Error, %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		if len(modelProduct) == 0 {
			logger.GlobaLogger.Errorf("%v - Get product Preferentail: not found", req.TransactionID)
			return nil, errors.New(common.ReasonNotFound.Code())
		}
		u.helperRedis.SetInterface(ctx, keyCache, modelProduct,
			time.Duration(u.cfg.RedisTimeDuration.CacheTimeProductPreferentail)*time.Second)
	}
	product = make([]*dto.ProductPreferentailResponseDTO, len(modelProduct))
	for i, item := range modelProduct {
		tag := dto.ProductPreferentailTag{
			New: item.Tag,
		}
		amount := dto.ProductPreferentailAmount{
			Sell:  item.SellPrice,
			Price: item.Price,
		}
		product[i] = &dto.ProductPreferentailResponseDTO{
			ID:               item.ID,
			Content:          item.Description,
			Name:             item.Name,
			Tag:              tag,
			ImageID:          item.ImageID,
			Amount:           amount,
			ShortDescription: item.ShortDescription.String,
		}
	}
	return product, nil
}

func (u *productUsecase) GetProductDetail(ctx context.Context,
	req *dto.GetProductDetailRequestDTO) (res *dto.GetProductDetailResponseDTO, err error) {
	var (
		moProduct *model.ProductModel
	)
	res = &dto.GetProductDetailResponseDTO{}
	keyCache := fmt.Sprintf(common.KeyCacheGetProductDetail, req.ID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, res)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get detail product in redis.", req.TransactionID)
		res = resInter.(*dto.GetProductDetailResponseDTO)
		return res, nil
	}
	moProduct, err = u.productRepository.GetProductDetail(ctx, req.ID)
	if err == sql.ErrNoRows {
		logger.GlobaLogger.Errorf("%v: Error while get detail product not found", req.TransactionID)
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get detail product: err = %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	res = &dto.GetProductDetailResponseDTO{
		ProductDetail: dto.ProductDetail{
			ID:               moProduct.ID,
			Name:             moProduct.Name,
			Price:            moProduct.Price,
			Vote:             moProduct.Vote.String,
			CategoryName:     moProduct.CategoryName,
			Sku:              moProduct.Sku,
			SellPrice:        moProduct.SellPrice,
			ShortDescription: moProduct.ShortDescription.String,
			Quantity:         moProduct.Quantity,
			UpdatedAt:        moProduct.UpdatedAt,
			IsActive:         moProduct.IsActive == "1",
			CreatedAt:        moProduct.CreatedAt,
			CategoryID:       moProduct.CategoryID,
			Description:      moProduct.Description,
			CreatedBy:        moProduct.CreatedBy.String,
			UpdatedBy:        moProduct.UpdatedBy.String,
			StatusOfProduct:  moProduct.StatusOfProduct,
		},
	}
	u.helperRedis.Set(ctx, keyCache, res, time.Duration(120)*time.Second)
	return res, nil
}
