package model

type (
	Chatroom struct {
		ID          uint32
		CandidateID uint32
		EmployerID  uint32
		Type        uint32
		JobID       uint32
		Name        string
		Alias       string
	}
)
