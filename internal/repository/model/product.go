package model

import "database/sql"

type (
	ProductPreferentailModel struct {
		ID               uint32
		Description      string
		Name             string
		SellPrice        int64
		Price            int64
		Tag              string
		ImageID          string
		ShortDescription sql.NullString
	}

	ProductModelReqeust struct {
		CategoryId uint32
		Sex        string
		ID         uint32
	}

	ProductModel struct {
		ID               uint32
		Name             string
		Price            int64
		SellPrice        int64
		Tag              string
		Vote             sql.NullString
		CategoryName     string
		UpdatedAt        string
		CreatedAt        string
		UpdatedBy        sql.NullString
		CreatedBy        sql.NullString
		CategoryID       uint32
		IsActive         string
		Description      string
		Quantity         uint32
		Sku              string
		StatusOfProduct  string
		ImageID          string
		ShortDescription sql.NullString
	}
)
