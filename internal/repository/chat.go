package repository

import (
	"errors"
	"shop-back-client/internal/common"
	"shop-back-client/internal/helper/db"
	"shop-back-client/internal/repository/model"
	"strings"

	"golang.org/x/net/context"
)

type (
	// ChatRepository declare all func in comment repository
	ChatRepository interface {
		CreateChatroom(ctx context.Context, req *model.Chatroom) error
	}

	chatRepository struct {
		dbHelper db.DBHelper
	}
)

// NewChatRepository func new instance comment repository
func NewChatRepository(dbHelper db.DBHelper) ChatRepository {
	return &chatRepository{
		dbHelper: dbHelper,
	}
}

func (r *chatRepository) CreateChatroom(ctx context.Context, req *model.Chatroom) error {
	var (
		query strings.Builder
		agrs  []interface{}
	)
	query.WriteString(`insert into chatrooms (name, alias, candidate_id, employer_id, type ) values (?, ?, ?, ?, ?)`)
	agrs = append(agrs, req.Name, req.Alias, req.CandidateID, req.EmployerID, req.Type)
	result, err := r.dbHelper.Open().Exec(query.String(), agrs...)
	if err != nil {
		return err
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}
