package helper

import (
	"github.com/gogo/protobuf/proto"
	"github.com/jinzhu/copier"
)

type (
	PbCopier interface {
		ToPb(to proto.Message, from interface{})
		FromPb(to interface{}, from proto.Message)
	}

	ModelCopier interface {
		CopyFromModel(to interface{}, from interface{})
		CopyToModel(to interface{}, from interface{})
	}

	pbCopier struct {
	}

	modelCopier struct {
	}
)

func NewPbCopier() PbCopier {
	return &pbCopier{}
}

func (s *pbCopier) ToPb(to proto.Message, from interface{}) {
	err := copier.Copy(to, from)
	if err != nil {
		return
	}
}

func (s *pbCopier) FromPb(to interface{}, from proto.Message) {
	_ = copier.Copy(to, from)
}

func NewModelCopier() ModelCopier {
	return &modelCopier{}
}

func (s *modelCopier) CopyToModel(to, from interface{}) {
	_ = copier.Copy(to, from)
}

func (s *modelCopier) CopyFromModel(to, from interface{}) {
	_ = copier.Copy(to, from)
}
