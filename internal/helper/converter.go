package helper

import "github.com/gogo/protobuf/proto"

type (
	PbcConverter interface {
		InterfaceToPb(to proto.Message, from interface{})
		PbToInterface(to interface{}, from proto.Message)
	}
)
