package helper

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

const (
	username = "root"
	password = "1"
	hostname = "127.0.0.1:3306"
)

// dsn connect to db
func dsn(dbName string) string {
	return fmt.Sprintf("%s:%s@%s/%s", username, password, hostname, dbName)
	// connectionString := fmt.Sprintf("%v/%v@%v:%v/%v", username, password, host, port, database)
}

// OpenConnectDB connect to db
func OpenConnectDB() (sql.DB, error) {
	db, err := sql.Open("mysql", dsn("shop"))
	if err != nil {
		return sql.DB{}, err
	}
	defer db.Close()
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return *db, nil
}
