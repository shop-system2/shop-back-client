package cache

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"time"

	"github.com/go-redis/redis"
)

type redisHelper struct {
	client *redis.Client
}

func (h *redisHelper) SetInterface(ctx context.Context, key string, value interface{}, time time.Duration) (err error) {
	jsonValue, err := json.Marshal(value)
	if err != nil {
		fmt.Println("Marshal value:", err)
		return err
	}

	err = h.client.Set(key, jsonValue, time).Err()
	if err != nil {
		fmt.Println("setRedis err:", err)
		return err
	}
	return nil
}

func (h *redisHelper) Set(ctx context.Context, key string, value interface{}, time time.Duration) (err error) {
	jsonValue, err := json.Marshal(value)
	if err != nil {
		fmt.Println("Marshal value:", err)
		return err
	}

	err = h.client.Set(key, jsonValue, time).Err()
	if err != nil {
		fmt.Println("setRedis err:", err)
		return err
	}
	return nil
}

func (h *redisHelper) GetInterface(ctx context.Context, key string, inputValue interface{}) (value interface{}, err error) {
	data, err := h.client.Get(key).Result()
	if err != nil {
		return nil, err
	}
	typeValue := reflect.TypeOf(inputValue)
	kind := typeValue.Kind()
	var outData interface{}
	switch kind {
	case reflect.Ptr, reflect.Struct, reflect.Slice:
		outData = reflect.New(typeValue).Interface()
	default:
		outData = reflect.Zero(typeValue).Interface()
	}
	err = json.Unmarshal([]byte(data), &outData)
	if err != nil {
		return nil, err
	}
	switch kind {
	case reflect.Ptr, reflect.Struct, reflect.Slice:
		outDataValue := reflect.ValueOf(outData)
		if reflect.Indirect(reflect.ValueOf(outDataValue)).IsZero() {
			return nil, errors.New("Get redis nill result")
		}
		if outDataValue.IsZero() {
			return outDataValue.Interface(), nil
		}
		return outDataValue.Elem().Interface(), nil
	}
	var outValue interface{} = outData
	if reflect.TypeOf(outData).ConvertibleTo(typeValue) {
		outValueConverted := reflect.ValueOf(outData).Convert(typeValue)
		outValue = outValueConverted.Interface()
	}
	return outValue, nil
}

func (h *redisHelper) GetString(ctx context.Context, key string, inputValue string) (outValue string, err error) {
	outValue, err = h.client.Get(key).Result()
	if err != nil {
		return "", err
	}
	return outValue, nil
}

func (h *redisHelper) SetString(ctx context.Context, key string, inputValue string, time time.Duration) (err error) {
	err = h.client.Set(key, inputValue, time).Err()
	if err != nil {
		fmt.Println("setRedis err:", err)
		return err
	}
	return nil
}

func (h *redisHelper) Del(ctx context.Context, key string) (err error) {
	_, err = h.client.Del(key).Result()
	if err != nil {
		return err
	}
	return nil
}

func (h *redisHelper) Expire(ctx context.Context, key string, time time.Duration) (err error) {
	_, err = h.client.Expire(key, time).Result()
	if err != nil {
		return err
	}
	return nil
}

func (h *redisHelper) Exists(ctx context.Context, key string) error {
	indicator, err := h.client.Exists().Result()
	if err != nil {
		return err
	}
	if indicator == 0 {
		return redis.Nil
	}
	return nil
}

func (h *redisHelper) DelMulti(ctx context.Context, keys ...string) error {
	var err error
	pipeline := h.client.TxPipeline()
	pipeline.Del(keys...)
	_, err = pipeline.Exec()
	return err
}

func (h *redisHelper) GetKeysByPattern(ctx context.Context, pattern string, cursor uint64, limit int64) ([]string, uint64, error) {
	return h.client.Scan(cursor, pattern, limit).Result()
}
func (h *redisHelper) SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) (isSucces bool, err error) {
	data, err := json.Marshal(value)
	if err != nil {
		return false, err
	}
	isSucces, err = h.client.SetNX(key, string(data), expiration).Result()
	if err != nil {
		return false, err
	}
	return isSucces, nil
}
func (h *redisHelper) RenameKey(ctx context.Context, oldkey, newkey string) error {
	var err error
	_, err = h.client.Rename(oldkey, newkey).Result()
	return err
}
func (h *redisHelper) GetStrLenght(ctx context.Context, key string) (int64, error) {
	return h.client.StrLen(key).Result()
}

func (h *redisHelper) GetType(ctx context.Context, key string) (string, error) {
	return h.client.Type(key).Result()
}

func (h *redisHelper) DebugObjectByKey(ctx context.Context, key string) (string, error) {
	return h.client.DebugObject(key).Result()
}

func (h *redisHelper) TimeExpire(ctx context.Context, key string) (time.Duration, error) {
	return h.client.TTL(key).Result()
}

func (h *redisHelper) GetMulti(ctx context.Context, data interface{}, keys ...string) (result []interface{}, err error) {
	var (
		cmds []redis.Cmder
	)
	p := h.client.Pipeline()
	p.MGet(keys...)
	cmds, err = p.Exec()
	if err != nil {
		return nil, err
	}
	for _, cmd := range cmds {
		if slice, ok := cmd.(*redis.SliceCmd); ok {
			resultItem, err := slice.Result()
			if err != nil {
				return nil, err
			}
			if len(resultItem) == 0 {
				continue
			}

			// get first one
			result = append(result, resultItem...)
		}

	}
	return result, nil
}
