package helper

import (
	"database/sql"
	"fmt"
)

// DBHelper declare all func
type DBHelper interface {
	Open() *sql.DB
	Close() error
	Begin() (*sql.Tx, error)
	Commit(tx *sql.Tx) error
	Rollback(tx *sql.Tx) error
}
type dbHelper struct {
	db *sql.DB
}

func initMysql(host, port, user, pass, database string) (*sql.DB, error) {
	connectString := fmt.Sprintf("%v/%v@%v:%v/%v", user, pass, host, port, database)
	db, err := sql.Open("mysql", connectString)
	if err != nil {
		return nil, err
	}
	if err := db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}

// NewDBHelper create install db
func NewDBHelper(host, port, user, pass, database string) DBHelper {
	db, err := initMysql(host, port, user, pass, database)
	if err != nil {
		return nil
	}
	return &dbHelper{
		db: db,
	}
}

func (h *dbHelper) Open() *sql.DB {
	return h.db
}

func (h *dbHelper) Close() error {
	return h.db.Close()
}

func (h *dbHelper) Begin() (*sql.Tx, error) {
	return h.db.Begin()
}

func (h *dbHelper) Commit(tx *sql.Tx) error {
	return tx.Commit()
}

func (h *dbHelper) Rollback(tx *sql.Tx) error {
	return tx.Rollback()
}
