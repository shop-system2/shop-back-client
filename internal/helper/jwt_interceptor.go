package helper

import (
	"google.golang.org/grpc"
)

const (
	// () : group string
	// * : averything
	// \\S : space white
	patternStr = "^Bearer (\\S*)"
)

// JWTAuthenticationInterceptor fuc
func JWTAuthenticationInterceptor() grpc.UnaryServerInterceptor {
	// pattern, err := regexp.Compile(patternStr)
	// if err != nil {
	// 	fmt.Println("err pattern")
	// }
	// return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	// 	fmt.Println("%v", pattern)
	// 	md, ok := metadata.FromIncomingContext(ctx)
	// 	if !ok {
	// 		return nil, nil
	// 	}
	// 	authorization, ok := md["authorization"]
	// 	if ok {
	// 		fmt.Println("authorization", authorization)
	// 	}
	// 	return nil, nil
	// }
	return nil
}
