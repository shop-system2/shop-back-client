package db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type mongoHelper struct {
	db *sql.DB
}

// mongoconnect connect to db
func mongoconnect(dbName, username, password, hostname string) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbName)
}

func InitMongoDB(host string, port int, username, passsword, database string) (*sql.DB, error) {
	// mongo
	client, err := mongo.NewClient(options.Client().ApplyURI("<ATLAS_URI_HERE>"))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(ctx)
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	databases, err := client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(databases)
	// end mongo
	hostPort := fmt.Sprintf("%v:%v", host, port)
	connectString := mongoconnect(database, username, passsword, hostPort)
	db, err := sql.Open("Mongo", connectString)
	if err != nil {
		fmt.Println("init Mongo error:", err)
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		fmt.Println("ping Mongo error:", err)
		return nil, err
	}
	return db, nil
}

func NewMongoHelper(host string, port int, username, password, database string) DBHelper {
	db, err := InitMongoDB(host, port, username, password, database)
	if err != nil {
		fmt.Println("failed to init Mongo")
	}
	return &mongoHelper{
		db: db,
	}
}

func (h *mongoHelper) Open() *sql.DB {
	return h.db
}

func (h *mongoHelper) Close() error {
	return h.db.Close()
}

func (h *mongoHelper) Begin() (*sql.Tx, error) {
	return h.db.Begin()
}

func (h *mongoHelper) Commit(tx *sql.Tx) error {
	return tx.Commit()
}

func (h *mongoHelper) Rollback(tx *sql.Tx) error {
	return tx.Rollback()
}
