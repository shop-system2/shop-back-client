package migrations

import (
	"fmt"
	"shop-back-client/internal/helper"

	"github.com/spf13/cobra"
)

var migrateUpCmd = &cobra.Command{
	Use:   "up",
	Short: "run up migrations",
	Run: func(cmd *cobra.Command, args []string) {
		step, err := cmd.Flags().GetInt("step")
		if err != nil {
			fmt.Println("Unable to read flag `step`")
			return
		}
		dbs, errConnectDB := helper.OpenConnectDB()
		if errConnectDB != nil {
			fmt.Println("errConnectDB:", errConnectDB)
			return
		}
		migrator, err := InitDB(&dbs)
		if err != nil {
			fmt.Println("Unable to fetch migrator")
			return
		}

		err = migrator.MigrateUp(step)
		if err != nil {
			fmt.Println("Unable to run `up` migrations")
			return
		}
	},
}

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "database migrations tool",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("migration cmd", cmd)
	},
}

var migrateDownCmd = &cobra.Command{
	Use:   "down",
	Short: "run down migrations",
	Run: func(cmd *cobra.Command, args []string) {

		step, err := cmd.Flags().GetInt("step")
		if err != nil {
			fmt.Println("Unable to read flag `step`")
			return
		}

		dbs, errConnectDB := helper.OpenConnectDB()
		if errConnectDB != nil {
			fmt.Println("errConnectDB:", errConnectDB)
			return
		}

		migrator, err := InitDB(&dbs)
		if err != nil {
			fmt.Println("Unable to fetch migrator")
			return
		}

		err = migrator.MigrateDown(step)
		if err != nil {
			fmt.Println("Unable to run `down` migrations")
			return
		}
	},
}

var rootCmd = &cobra.Command{
	Use:   "app",
	Short: "Application Description",
}

var migrateCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "create a new empty migrations file",
	Run: func(cmd *cobra.Command, args []string) {
		name, err := cmd.Flags().GetString("name")
		if err != nil {
			fmt.Println("Unable to read flag `name`", err.Error())
			return
		}
		err = Create(name)
		if err != nil {
			fmt.Println("Unable to create migration", err.Error())
			return
		}
	},
}
