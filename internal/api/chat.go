package api

import (
	"context"
	pb "shop-back-client/genproto"
	"shop-back-client/internal/common"
	"shop-back-client/internal/dto"
	util "shop-back-client/internal/util"
)

func (a *api) CreateChatroom(ctx context.Context,
	req *pb.CreateChatroomRequest) (resp *pb.CreateChatroomResponse, err error) {
	var (
		reqDTO = &dto.Chatroom{}
	)
	resp = &pb.CreateChatroomResponse{}
	a.pbCopier.FromPb(reqDTO, req)
	err = a.chatUsecase.CreateChatroom(ctx, reqDTO)
	if err != nil {
		util.SetErrorToResponse(err, resp)
		return resp, nil
	}
	resp.StatusCode = common.ACCEPT
	return resp, nil
}
func (a *api) SendMessage(ctx context.Context,
	req *pb.SendMessageRequest) (resp *pb.SendMessageResponse, err error) {
	// var (
	// 	reqDTO = &dto.Chatroom{}
	// )
	resp = &pb.SendMessageResponse{}
	// a.pbCopier.FromPb(reqDTO, req)
	// err = a.chatUsecase.CreateChatroom(ctx, reqDTO)
	// if err != nil {
	// 	util.SetErrorToResponse(err, resp)
	// 	return resp, nil
	// }
	resp.StatusCode = common.ACCEPT
	return resp, nil
}
