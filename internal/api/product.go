package api

import (
	"context"
	pb "shop-back-client/genproto"
	"shop-back-client/internal/common"
	"shop-back-client/internal/dto"
	util "shop-back-client/internal/util"
)

func (a *api) GetProductPreferentail(ctx context.Context,
	req *pb.GetProductPreferentailRequest) (resp *pb.GetProductPreferentailResponse, err error) {
	var (
		reqDTO = &dto.GetProductPreferentailRequestDTO{}
	)
	resp = &pb.GetProductPreferentailResponse{}
	reqDTO.TransactionID = req.TransactionID
	resDTO, err := a.productUsecase.GetProductPreferentail(ctx, reqDTO)
	if err != nil {
		util.SetErrorToResponse(err, resp)
		return resp, nil
	}
	for _, item := range resDTO {
		tag := &pb.ProductTag{
			New:   item.Tag.New,
			Promo: item.Tag.Promo,
			Best:  item.Tag.Best,
		}
		amount := &pb.ProductAmount{
			Price: item.Amount.Price,
			Sale:  item.Amount.Sell,
		}
		resp.ProductPreferentails = append(resp.ProductPreferentails, &pb.ProductPerferentail{
			Name:             item.Name,
			Tag:              tag,
			Amount:           amount,
			ProductID:        item.ID,
			Content:          item.Content,
			ImagesID:         item.ImageID,
			ShortDescription: item.ShortDescription,
		})
	}
	resp.StatusCode = common.ACCEPT
	return resp, nil
}

func (a *api) GetProductTrending(ctx context.Context,
	req *pb.GetProductTrendingRequest) (resp *pb.GetProductTrendingResponse, err error) {
	var (
		reqDTO = &dto.GetProductTrendingRequestDTO{}
	)
	resp = &pb.GetProductTrendingResponse{}
	a.pbCopier.FromPb(reqDTO, req)
	resDTO, err := a.productUsecase.GetProductTrending(ctx, reqDTO)
	if err != nil {
		util.SetErrorToResponse(err, resp)
		return resp, nil
	}
	a.pbCopier.ToPb(resp, resDTO)
	resp.StatusCode = common.ACCEPT
	return resp, nil
}

func (a *api) GetProductDetail(ctx context.Context,
	req *pb.GetProductDetailRequest) (resp *pb.GetProductDetailResponse, err error) {
	resp = &pb.GetProductDetailResponse{}
	reqDTO := &dto.GetProductDetailRequestDTO{}
	a.pbCopier.FromPb(reqDTO, req)
	product, err := a.productUsecase.GetProductDetail(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		resp.StatusCode = common.FAILED
		resp.ReasonCode = common.ParseError(err).Code()
		resp.ReasonMessage = errMessage
		return resp, nil
	}
	a.pbCopier.ToPb(resp, product)
	resp.StatusCode = common.ACCEPT
	return resp, nil
}
