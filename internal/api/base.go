package api

import (
	"encoding/json"
	"net/http"
	"shop-back-client/internal/helper"
	"shop-back-client/internal/usecase"
)

type (
	objResponse struct {
		Code    int
		Message string
	}

	jwtResponse struct {
		Code    int
		Message string
		Jwt     string
	}
)

// ResponseFail sdf
func ResponseFail(writer http.ResponseWriter, code int, message string) {
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(objResponse{code, message})
}

// ResponseSuccess sdf
func ResponseSuccess(writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(objResponse{200, "sescuessfull"})
}

// ResponseJwt sdf
func ResponseJwt(writer http.ResponseWriter, jwt string) {
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(jwtResponse{200, "", jwt})
}

type (
	API interface {
	}
	api struct {
		productUsecase usecase.ProductUsecase
		chatUsecase    usecase.ChatUsecase
		pbCopier       helper.PbCopier
	}
)

func NewAPI(
	pbCopier helper.PbCopier,
	productUsecase usecase.ProductUsecase,
	chatUsecase usecase.ChatUsecase,
) API {
	return &api{
		pbCopier:       pbCopier,
		productUsecase: productUsecase,
		chatUsecase:    chatUsecase,
	}
}
