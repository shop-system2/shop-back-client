package dto

type (
	Chatroom struct {
		TransactionID string
		CandidateID   uint32
		EmployerID    uint32
		Type          uint32 // 0 is single, 1 is group
		Name          string
		Alias         string
	}
)
