package dto

type (

	// BaseReponseDTO represent common reponse dto
	BaseReponseDTO struct {
		StatusCode string
		ReasonCode string
		Message    string
	}

	// SmptServer anc
	SmptServer struct {
		Host string
		Port string
	}
	ResponseBaseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		TransactionID string
	}
)
