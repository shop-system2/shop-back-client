package dto

type (
	GetProductDetailRequestDTO struct {
		TransactionID string
		ID            uint32
	}

	ProductDetail struct {
		ID               uint32 `json:"id"`
		Name             string `json:"name"`
		Vote             string `json:"vote"`
		CategoryName     string `json:"category_name"`
		UpdatedAt        string `json:"updated_at"`
		CreatedAt        string `json:"created_at"`
		CategoryID       uint32 `json:"category_id"`
		Description      string
		TransactionID    string
		CreatedBy        string
		UpdatedBy        string
		Quantity         uint32
		Sku              string
		IsActive         bool
		SellPrice        int64
		Price            int64 `json:"price"`
		ShortDescription string
		StatusOfProduct  string
	}
	GetProductDetailResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		ProductDetail ProductDetail
	}

	GetProductRequestDTO struct {
		TransactionID string
		CategoryID    uint32
		Sex           string
	}
	GetProductPreferentailRequestDTO struct {
		TransactionID string
	}
	ProductPreferentailAmount struct {
		Sell  int64
		Price int64
	}
	ProductPreferentailTag struct {
		New   string
		Promo string
		Best  string
	}
	ProductPreferentailResponseDTO struct {
		ID               uint32
		Content          string
		Name             string
		Amount           ProductPreferentailAmount
		Tag              ProductPreferentailTag
		ImageID          string
		ShortDescription string
	}

	ProductAmount struct {
		Sell  int64
		Price int64
	}
	ProductTag struct {
		New   string
		Promo string
		Best  string
	}

	ProductTrendingResponseDTO struct {
		ResponseBaseDTO
		ProductTrending []*ProductDTO
	}

	ProductDTO struct {
		ID      uint32
		Content string
		Name    string
		Amount  ProductAmount
		Tag     ProductTag
		ImageID string
	}

	// *********** old code

	// NewProductRequestDTO represent new product dto
	NewProductRequestDTO struct {
		Name  string
		Price int64
		Vote  int
	}
	// PostNewProductRequestDTO represent new product dto
	PostNewProductRequestDTO struct {
		Name          string
		Price         int64
		Vote          int
		CategoryID    int
		Description   string
		CreatedAt     string
		UpdatedAt     string
		TransactionID string
		CreatedBy     string
		UpdatedBy     string
	}
	//PostNewProductResponseDTO represent
	PostNewProductResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}

	//ProductsDTO represent list product in dto
	ProductsDTO struct {
		Name         string `json:"name"`
		Price        int64  `json:"price"`
		Vote         int    `json:"vote"`
		CategoryName string `json:"category_name"`
		CreatedAt    string `json:"created_at"`
	}

	//ProductsResponseDTO represent list product in dto
	ProductsResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Products   []ProductsDTO `json:"products"`
	}
	// GetListProductRequestDTO represent
	GetListProductRequestDTO struct {
		TransactionID string
		CurrentPage   uint32
		Limit         uint32
		IsActive      bool
		UserID        uint32
		Offset        uint32
	}
	SearchProductRequestDTO struct {
		Name          string `json:"name"`
		Code          string `json:"code"`
		TransactionID string
	}

	// GetListProductResponseDTO represent
	GetListProductResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Products   []ProductsDTO `json:"products"`
	}
	// PutUpdateProductRequestDTO represent
	PutUpdateProductRequestDTO struct {
		Name          string `json:"name"`
		Price         int64  `json:"price"`
		Vote          int    `json:"vote"`
		ID            int    `json:"id"`
		Description   string
		CategoryID    uint32
		UpdatedBy     string
		TransactionID string
	}
	// PutUpdateProductResponseDTO represent
	PutUpdateProductResponseDTO struct {
		StatusCode string `json:"status_code"`
	}
	// DeleteProductRequestDTO represent
	DeleteProductRequestDTO struct {
		ID            uint32 `json:"id"`
		TransactionID string
	}
	// DeleteProductResponseDTO represent
	DeleteProductResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}

	DoActiveProductRequestDTO struct {
		ProductID     uint32
		IsActive      bool
		TransactionID string
	}

	GetProductTrendingRequestDTO struct {
		TransactionID string
		CategoryID    uint32
		Sex           string
		ID            uint32
	}
)
