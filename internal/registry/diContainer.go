package registry

import (
	"shop-back-client/config"
	"shop-back-client/internal/api"
	"shop-back-client/internal/helper"
	"shop-back-client/internal/helper/cache"
	"shop-back-client/internal/helper/db"
	"shop-back-client/internal/repository"

	"shop-back-client/internal/usecase"

	"go-libs/logger"
	"sync"

	"github.com/sarulabs/di"
)

var (
	buildOnce sync.Once
	builder   *di.Builder
	container di.Container
)

const (

	// service gateway
	ServiceIdentityManagement string = "ServiceIdentityManagement"

	// api
	APIDIName string = "API"

	// External
	RedisCacheHelper      string = "RedisCacheHelper"
	ExternalDBMysqlHelper string = "ExternalDBMysqlHelper"

	// ConfigApp a
	ConfigApp string = "ConfigApp"

	// UserUsecase
	UserUsecase     string = "UserUsecase"
	UserRespository string = "UserRespository"

	// CommentUsecase
	CommentUsecase     string = "CommentUsecase"
	CommentRespository string = "CommentRespository"

	// HelperPbCopier
	HelperPbCopier     string = "HelperPbCopier"
	HelperModelCopier  string = "HelperModelCopier"
	HelperDB           string = "HelperDB"
	RedisCacheHelperss string = "RedisCacheHelper"
	LoggerHelperIDName string = "LoggerHelper"

	// usecase
	ProductUsecase string = "ProductUsecase"
	ChatUsecase    string = "ChatUsecase"

	// repository
	ProductRespository string = "ProductRespository"
	ChatRespository    string = "ChatRespository"

	// adapter
	NotificationAdapterNameDI string = "NotificationAdapter"
)

// BuildDiContainer represent DiContainer
func BuildDiContainer() {
	builder, _ = di.NewBuilder()

	if err := buildRepository(); err != nil {
		panic(err)
	}
	if err := buildUsecase(); err != nil {
		panic(err)
	}
	if err := buildAPI(); err != nil {
		panic(err)
	}

	if err := buildAPIUsing(); err != nil {
		panic(err)
	}
	if err := buildHelper(); err != nil {
		panic(err)
	}
	if err := ExternalHelperBuild(); err != nil {
		panic(err)
	}

	container = builder.Build()
}

// buildAPIUsing create instance for api
func buildAPIUsing() error {
	defs := []di.Def{}
	APIDef := di.Def{
		Name:  APIDIName,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			productUsecase := ctn.Get(ProductUsecase).(usecase.ProductUsecase)
			helperPbCopier := ctn.Get(HelperPbCopier).(helper.PbCopier)
			chatUsecase := ctn.Get(ChatUsecase).(usecase.ChatUsecase)
			return api.NewAPI(helperPbCopier, productUsecase, chatUsecase), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, APIDef)
	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

// buildAPI create instance for api
func buildAPI() error {
	defs := []di.Def{}

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func ExternalHelperBuild() error {
	defs := []di.Def{}
	cacheHelperExternal := di.Def{
		Name:  RedisCacheHelper,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get("ConfigApp").(*config.Config)
			return cache.NewRedisInstance(cfg.Redis.Addrs), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, cacheHelperExternal)

	dbMysqlHelperExternal := di.Def{
		Name:  ExternalDBMysqlHelper,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			mysqlDB := db.NewMysqlDBHelper(cfg.Mysql.Host, cfg.Mysql.Port, cfg.Mysql.Username, cfg.Mysql.Password, cfg.Mysql.Database)
			return mysqlDB, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, dbMysqlHelperExternal)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildUsecase() error {
	defs := []di.Def{}

	productUsecaseDef := di.Def{
		Name:  ProductUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			productRepository := ctn.Get(ProductRespository).(repository.ProductRepository)
			helperPbCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			return usecase.NewProductUsecase(cfg, productRepository, helperRedis, helperPbCopier), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, productUsecaseDef)

	chatUsecaseDef := di.Def{
		Name:  ChatUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			chatRepository := ctn.Get(ChatRespository).(repository.ChatRepository)
			helperPbCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			logger := ctn.Get(LoggerHelperIDName).(logger.Logger)
			return usecase.NewChatUsecase(cfg, chatRepository, helperRedis, helperPbCopier, logger), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, chatUsecaseDef)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildHelper() error {
	defs := []di.Def{}
	configPbCopier := di.Def{
		Name:  ConfigApp,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			config, err := config.LoadConfig()
			if err != nil {
				return nil, err
			}
			return config, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, configPbCopier)

	helperPbCopier := di.Def{
		Name:  HelperPbCopier,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			return helper.NewPbCopier(), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, helperPbCopier)

	helperModelCopier := di.Def{
		Name:  HelperModelCopier,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			return helper.NewModelCopier(), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, helperModelCopier)

	loggerDIName := di.Def{
		Name:  LoggerHelperIDName,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {

			cfg := ctn.Get(ConfigApp).(*config.Config)
			apiLogger := logger.Newlogger(cfg.Logger.Mode, cfg.Logger.Level, cfg.Logger.Encoding)
			return apiLogger, nil

			// cfg := ctn.Get(ConfigApp).(*config.Config)
			// apiLogger := logger.NewAPILogger()
			// apiLogger.InitLogger(cfg.Mode, cfg.Logger.Level, cfg.Logger.Encoding)
			// return apiLogger, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, loggerDIName)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildRepository() error {
	defs := []di.Def{}

	productRepository := di.Def{
		Name:  ProductRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewProductRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, productRepository)

	chatRepository := di.Def{
		Name:  ChatRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewChatRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, chatRepository)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

// GetDependency represent container
func GetDependency(depen string) interface{} {
	return container.Get(depen)
}
