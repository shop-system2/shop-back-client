module shop-back-client

go 1.16

replace go-libs => gitlab.com/shop-system2/go-libs v0.0.0-20211031073519-dbce0dbff414

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20190118093823-f849b5445de4
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/jinzhu/copier v0.2.8
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/onsi/ginkgo v1.16.1 // indirect
	github.com/prometheus/client_golang v1.5.1
	github.com/rakyll/statik v0.1.7
	github.com/rs/cors v1.7.0
	github.com/sarulabs/di v2.0.0+incompatible
	github.com/smartystreets/assertions v1.0.0 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/tealeg/xlsx/v3 v3.2.3
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	go-libs v0.0.0-20211031073519-dbce0dbff414
	go.mongodb.org/mongo-driver v1.7.2
	go.uber.org/zap v1.14.1
	golang.org/x/crypto v0.0.0-20210415154028-4f45737414dc
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d
	google.golang.org/genproto v0.0.0-20210303154014-9728d6b83eeb
	google.golang.org/grpc v1.38.0
)
