package main

import (
	"context"
	"fmt"
	"go-libs/logger"
	"net"
	"net/http"
	"os"
	"os/signal"
	_ "shop-back-client/cmd/server/statik"
	"shop-back-client/config"
	pb "shop-back-client/genproto"

	"shop-back-client/internal/registry"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"

	helper_grpc "shop-back-client/internal/helper/grpc"

	"github.com/rakyll/statik/fs"
	"github.com/rs/cors"
	"google.golang.org/grpc"
)

func main() {
	registry.BuildDiContainer()
	cfg := registry.GetDependency("ConfigApp").(*config.Config)
	var grpcServer *grpc.Server
	var gateway *http.Server

	// init logger
	appLogger := registry.GetDependency("LoggerHelper").(logger.Logger)
	appLogger.Infof(
		"AppVersion: %s, LogLevel: %s, Mode: %s",
		cfg.AppVersion,
		cfg.Logger.Level,
		cfg.Mode,
	)

	// go routine grpc
	go func() {
		protoAPI := registry.GetDependency("API").(pb.APIServer)
		listener, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.GRPCAddress))
		if err != nil {
			appLogger.Fatalf("failed to listen: %v", err)
		}
		// var opts []grpc.ServerOption
		grpcServer = grpc.NewServer(
			grpc_middleware.WithUnaryServerChain(
				helper_grpc.SetUniqueIDUnaryInterceptor(), // gen unique id for each api
				grpc_validator.UnaryServerInterceptor(),   // add validator, purpose using in file proto
				// interceptor prometheus
				grpc_prometheus.UnaryServerInterceptor,
			),
		)
		pb.RegisterAPIServer(grpcServer, protoAPI)
		// register for prometheus
		grpc_prometheus.Register(grpcServer)

		if err := grpcServer.Serve(listener); err != nil {
			appLogger.Errorf("fail to grpcServer")
		}
	}()

	// go routine mux router
	statikFS, err := fs.New()
	if err != nil {
		return
	}
	mux := runtime.NewServeMux(
		runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{OrigName: true, EmitDefaults: true}),
	)
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	appLogger.Infof(fmt.Sprintf("start server-grpc:%v", cfg.GRPCAddress))
	err = pb.RegisterAPIHandlerFromEndpoint(ctx, mux, fmt.Sprintf(":%d", cfg.GRPCAddress), opts)
	if err != nil {
		return
	}

	httpMux := http.NewServeMux()
	// register
	httpMux.Handle("/metrics", promhttp.Handler())

	httpMux.Handle("/backclient/swagger-ui/", http.StripPrefix("/backclient/swagger-ui/", http.FileServer(statikFS)))
	httpMux.Handle("/", mux)

	// start handler cors
	var httpHandler http.Handler = httpMux
	if cfg.CORSLocal {
		corsHandler := cors.New(cors.Options{

			AllowedOrigins:   []string{"*"},
			AllowCredentials: true,
			AllowedMethods:   []string{"GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS"},
			AllowedHeaders:   []string{"*"},
			ExposedHeaders:   []string{},
			Debug:            true,
		})
		httpHandler = corsHandler.Handler(httpMux)
	}
	// end handler cors
	// http://172.29.164.164:11105/backclient/swagger-ui/#
	appLogger.Infof(fmt.Sprintf("start server-grpc:%v", cfg.HTTPAddress))
	gateway = &http.Server{
		Addr:    fmt.Sprintf(":%d", cfg.HTTPAddress),
		Handler: httpHandler,
	}

	go func() {
		if err := gateway.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			return
		}
	}()
	appLogger.Infof("****** START-SERVER-RUNNING ******")
	signalsGraceFull := make(chan os.Signal, 1)
	signalsShutdown := make(chan bool, 1)
	signal.Notify(signalsGraceFull, os.Interrupt)
	go func() {
		<-signalsGraceFull
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := gateway.Shutdown(ctx); err != nil {
			appLogger.Fatalf("server Shutdown Failed:%+s", err)
		}
		signalsShutdown <- false
	}()
	<-signalsShutdown
	appLogger.Infof("SHUTDOWN-SERVER")
}
