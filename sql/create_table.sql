
--  create table client:
create table chatrooms (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    alias VARCHAR(255),
    candidate_id int NOT NULL,
    employer_id int NOT NULL,
    type CHAR(1) DEFAULT '0' NOT NULL,  -- group = 1 
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    PRIMARY KEY (id)
);

select * from chatrooms;
insert into chatrooms(name,alias,candidate_id, employer_id) values ('room name 1','alias room 1',1,1);