package config

import (
	"bytes"
	"strings"

	"github.com/spf13/viper"
)

// yamlConfig config all variable in app
var yamlConfig = []byte(`
mode: development
cors_local: true
app_version: 1.0.0
grpc_address: 11104
http_address: 11105
redis_time_duration:
  cache_time_product_preferentail: 30
  cache_time_product: 60
mysql:
  username: root
  password: 1
  database: shop
  host: 127.0.0.1
  port: 3306
redis_setting:
  addrs:
  - localhost:6379
logger:
  mode: development
  disable_caller: false
  disable_stacktrace: false
  encoding: json
  level: debug
`)

type (
	// Config fsdf
	Config struct {
		CORSLocal         bool              `yaml:"cors_local" mapstructure:"cors_local"`
		Mode              string            `mapstructure:"mode"`
		AppVersion        string            `yaml:"app_version" mapstructure:"app_version"`
		HTTPAddress       int               `mapstructure:"http_address"`
		GRPCAddress       int               `mapstructure:"grpc_address"`
		RedisTimeDuration redisTimeDuration `mapstructure:"redis_time_duration"`
		Mysql             mysql             `mapstructure:"mysql"`
		Redis             redisIn           `yaml:"redis_setting" mapstructure:"redis_setting"`
		Logger            Logger            `yaml:"logger" mapstructure:"logger"`
	}
	// Logger struct {
	// 	Development       bool   `yaml:"development" mapstructure:"development"`
	// 	DisableCaller     bool   `yaml:"disable_caller" mapstructure:"disable_caller"`
	// 	DisableStacktrace bool   `yaml:"disable_stacktrace" mapstructure:"disable_stacktrace"`
	// 	Encoding          string `yaml:"encoding" mapstructure:"encoding"`
	// 	Level             string `yaml:"level" mapstructure:"level"`
	// }

	Logger struct {
		Mode              string `yaml:"mode" mapstructure:"mode"`
		DisableCaller     bool   `yaml:"disable_caller" mapstructure:"disable_caller"`
		DisableStacktrace bool   `yaml:"disable_stacktrace" mapstructure:"disable_stacktrace"`
		Encoding          string `yaml:"encoding" mapstructure:"encoding"`
		Level             string `yaml:"level" mapstructure:"level"`
	}
	redisIn struct {
		Addrs []string
	}
	redisTimeDuration struct {
		CacheTimeProductPreferentail uint32 `yaml:"cache_time_product_preferentail" mapstructure:"cache_time_product_preferentail"`
		CacheTimeProduct             uint32 `yaml:"cache_time_product" mapstructure:"cache_time_product"`
	}
	mysql struct {
		Username string
		Password string
		Database string
		Host     string
		Port     int
	}
	// report setting

)

// LoadConfig is func load config for app
func LoadConfig() (*Config, error) {
	var conf = &Config{}
	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(yamlConfig))
	if err != nil {
		return nil, err
	}
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "__"))
	viper.AutomaticEnv()
	err = viper.Unmarshal(&conf)
	if err != nil {
		return nil, err
	}
	return conf, nil
}
