FROM golang:1.15 as builder
WORKDIR /app
COPY . /app
ENV GO111MODULE on
ENV GOFLAGS=-mod=vendor
RUN go mod vendor
RUN go build -o study /app/cmd/main.go
FROM golang:1.15
WORKDIR /app
COPY --from=builder /app/study /app/golang.go
CMD ["./golang.go"]